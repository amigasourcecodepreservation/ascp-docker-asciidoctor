FROM ubuntu:latest 

ARG asciidoctor_version=2.0.10
ARG asciidoctor_pdf_version=1.5.0.rc.2

ENV ASCIIDOCTOR_VERSION=${asciidoctor_version} \
  ASCIIDOCTOR_PDF_VERSION=${asciidoctor_pdf_version}

# Installing package required for the runtime of
# any of the asciidoctor-* functionnalities

RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections


RUN apt-get update && apt-get -y install curl \
  ca-certificates \
  graphviz \
  inotify-tools \
  build-essential \ 
  make \
  flex \
  libffi-dev \
  libxml2-dev \
  libgdk-pixbuf2.0-dev \
  libcairo2-dev \
  libpango1.0-dev \
  fonts-lyx \
  bison \
  cmake \
  openjdk-8-jre \
# ttf-mscorefonts-installer \
  unzip 

# Installing Ruby Gems needed in the image
# including asciidoctor itself
RUN apt-get -y install ruby-full \
    ruby-dev \
  && gem install --no-document \
    "asciidoctor:${ASCIIDOCTOR_VERSION}" \
    asciidoctor-confluence \
    asciidoctor-diagram \
    asciidoctor-epub3:1.5.0.alpha.10 \
    asciidoctor-mathematical \
    asciimath \
    "asciidoctor-pdf:${ASCIIDOCTOR_PDF_VERSION}" \
    asciidoctor-revealjs \
    coderay \
    haml \
    rake \
    pygments.rb \
    rouge \
    slim \
    thread_safe \
    tilt 

# Installing Python dependencies for additional
# functionnalities as diagrams or syntax highligthing
RUN apt-get -y install \
    python-dev \
    python-pip \
  && pip install --no-cache-dir \
    actdiag \
    'blockdiag[pdf]' \
    nwdiag \
    Pygments \
    seqdiag 

#svgbob
RUN apt-get -y install cargo \
    && cargo install svgbob_cli 

ENV PATH="/root/.cargo/bin:${PATH}"

WORKDIR /documents
VOLUME /documents

CMD ["/bin/bash"]
